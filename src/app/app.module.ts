import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {A11yModule} from "@angular/cdk/a11y";
import {ClipboardModule} from "@angular/cdk/clipboard";
import {CdkStepperModule} from "@angular/cdk/stepper";
import {CdkTableModule} from "@angular/cdk/table";
import {CdkTreeModule} from "@angular/cdk/tree";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatBadgeModule} from "@angular/material/badge";
import {MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatDialogModule} from "@angular/material/dialog";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatChipsModule} from "@angular/material/chips";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatStepperModule} from "@angular/material/stepper";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule, MatRippleModule} from "@angular/material/core";
import {MatMenuModule} from "@angular/material/menu";
import {MatListModule} from "@angular/material/list";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatRadioModule} from "@angular/material/radio";
import {MatSelectModule} from "@angular/material/select";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatSliderModule} from "@angular/material/slider";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatTabsModule} from "@angular/material/tabs";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatTreeModule} from "@angular/material/tree";
import {OverlayModule} from "@angular/cdk/overlay";
import {PortalModule} from "@angular/cdk/portal";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {ReactiveFormsModule} from "@angular/forms";
import {TemperatureComponent} from './temperature/temperature.component';
import {PollutionComponent} from './pollution/pollution.component';
import {HumidityComponent} from './humidity/humidity.component';
import {HttpClientModule} from "@angular/common/http";
import AppService from "./app.service";
import {CommonModule} from "@angular/common";

@NgModule({
            exports: [],
            declarations: [
              AppComponent,
              TemperatureComponent,
              PollutionComponent,
              HumidityComponent
            ],
            imports: [
              BrowserModule,
              CommonModule,
              AppRoutingModule,
              HttpClientModule,
              MatFormFieldModule,
              MatSlideToggleModule,
              A11yModule,
              ClipboardModule,
              CdkStepperModule,
              CdkTableModule,
              CdkTreeModule,
              DragDropModule,
              MatAutocompleteModule,
              MatBadgeModule,
              MatBottomSheetModule,
              MatButtonModule,
              MatButtonToggleModule,
              MatCardModule,
              MatCheckboxModule,
              MatChipsModule,
              MatStepperModule,
              MatDatepickerModule,
              MatDialogModule,
              MatDividerModule,
              MatExpansionModule,
              MatGridListModule,
              MatIconModule,
              MatInputModule,
              MatListModule,
              MatMenuModule,
              MatNativeDateModule,
              MatPaginatorModule,
              MatProgressBarModule,
              MatProgressSpinnerModule,
              MatRadioModule,
              MatRippleModule,
              MatSelectModule,
              MatSidenavModule,
              MatSliderModule,
              MatSnackBarModule,
              MatSortModule,
              MatTableModule,
              MatTabsModule,
              MatToolbarModule,
              MatTooltipModule,
              MatTreeModule,
              OverlayModule,
              PortalModule,
              ScrollingModule,
              BrowserAnimationsModule,
              ReactiveFormsModule,
            ],
            providers: [
              AppService,
            ],
            bootstrap: [AppComponent]
          })
export class AppModule {
}
