import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {Injectable} from "@angular/core";
import Response from "./common/Response";
import {debounceTime} from "rxjs/operators";

@Injectable()
export default class AppService {

  currentState: BehaviorSubject<Response> = new BehaviorSubject<Response>(new Response());

  constructor(private client: HttpClient) {
  }

  toggleTemperatureEdit(): void {
    this.client
        .post<any>('/api/temperature/toggle', {})
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  togglePollutionEdit(): void {
    this.client
        .post<any>('/api/pollution/toggle', {})
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  toggleHumidityEdit(): void {
    this.client
        .post<any>('/api/humidity/toggle', {})
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  setTemperature(value: number): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.client
        .post<any>('/api/temperature', {value: value}, {headers: headers})
        .pipe(
          debounceTime(1000),
        )
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  setPollution(value: number): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.client
        .post<any>('/api/pollution', {value: value}, {headers: headers})
        .pipe(
          debounceTime(1000),
        )
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  setHumidity(value: number): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.client
        .post<any>('/api/humidity', {value: value}, {headers: headers})
        .pipe(
          debounceTime(1000),
        )
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  getCurrentState(): void {
    this.client
        .get<Response>('/api/currentState')
        .subscribe(currentState => this.setCurrentState(currentState));
  }

  private setCurrentState(currentState: Response) {
    this.currentState.next(currentState);
  }
}
