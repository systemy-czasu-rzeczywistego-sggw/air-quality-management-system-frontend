import {Component} from '@angular/core';
import Response from "./common/Response";
import AppService from "./app.service";
import {BehaviorSubject} from "rxjs";

@Component(
  {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
  },
)
export class AppComponent {

  currentState: BehaviorSubject<Response> = this.service.currentState;

  constructor(private service: AppService) {
    this.updateForEachTime();
  }

  private async updateForEachTime() {
    while (true) {
      this.service.getCurrentState();
      await this.sleep(1000);
    }
  }

  private sleep(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
