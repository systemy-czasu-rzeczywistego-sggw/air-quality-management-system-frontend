import {Component, Input} from '@angular/core';
import Data from "../common/Data";
import AppService from "../app.service";

@Component({
             selector: 'app-temperature',
             templateUrl: './temperature.component.html',
             styleUrls: ['./temperature.component.scss']
           })
export class TemperatureComponent {

  @Input()
  data: Data | undefined = new Data();

  min: number = 15;
  max: number = 25;

  constructor(private service: AppService) {
  }

  toggleEdit() {
    this.service.toggleTemperatureEdit();
  }

  setValue(event: any) {
    let newValue: number = event && event.target && event.target.value;
    if (newValue === undefined || newValue < this.min || newValue > this.max) {
      if (this.data) {
        this.data.expectedValue = newValue || 0;
      }
      return;
    }
    this.service.setTemperature(newValue);
  }

  isInvalid(): boolean {
    if (!this.data) {
      return false;
    }
    const expectedValue: number = this.data.expectedValue;
    return this.data.enabledAutoMode && (
      expectedValue < this.min || expectedValue > this.max
    );
  }
}
