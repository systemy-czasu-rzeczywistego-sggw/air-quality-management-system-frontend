import Data from "./Data";

export default class Response {

  temperature: Data;
  pollution: Data;
  humidity: Data;

  constructor(temperature?: Data, pollution?: Data, humidity?: Data) {
    this.temperature = temperature || new Data();
    this.pollution = pollution || new Data();
    this.humidity = humidity || new Data();
  }
}
