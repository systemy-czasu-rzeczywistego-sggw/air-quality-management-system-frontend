export default class Data {

  currentValue: number;
  realValueFromSensor: number
  expectedValue: number
  enabledAutoMode: boolean;

  constructor(currentValue?: number, realValueFromSensor?: number, expectedValue?: number, enabledAutoMode?: boolean) {
    this.currentValue = currentValue || 0;
    this.realValueFromSensor = realValueFromSensor || 0;
    this.expectedValue = expectedValue || 0;
    this.enabledAutoMode = enabledAutoMode || false;
  }
}
