import {Component, Input} from '@angular/core';
import Data from "../common/Data";
import AppService from "../app.service";

@Component({
             selector: 'app-humidity',
             templateUrl: './humidity.component.html',
             styleUrls: ['./humidity.component.scss']
           })
export class HumidityComponent {

  @Input()
  data: Data | undefined = new Data();

  min: number = 45;
  max: number = 60;

  constructor(private service: AppService) {
  }

  toggleEdit() {
    this.service.toggleHumidityEdit();
  }

  setValue(event: any) {
    let newValue: number = event && event.target && event.target.value;
    if (newValue === undefined || newValue < this.min || newValue > this.max) {
      if (this.data) {
        this.data.expectedValue = newValue || 0;
      }
      return;
    }
    this.service.setHumidity(newValue);
  }

  isInvalid(): boolean {
    if (!this.data) {
      return false;
    }
    const expectedValue: number = this.data.expectedValue;
    return this.data.enabledAutoMode && (expectedValue < this.min || expectedValue > this.max);
  }
}
